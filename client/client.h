#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QThread>
#include <QFile>


#include "clientworker.h"

class Client : public QObject
{
    Q_OBJECT
public:
    Client(const QString &configFName);
    ~Client();

private:
    void readCfgFile();

private slots:
    void packetReceived(int packetNum, int outOfPackets);

signals:
    void runWorker();

private:
    QString m_confFile;    // conf filename if passed by app param
    QString m_hostAddr;    // server IP or hostname
    uint    m_hostPort;    // server TCP port
    double  m_doubleValue; // value to be sent to server

    ClientWorker m_worker;
    QThread      m_workerThread;

    QFile m_outputFile;
};

#endif // CLIENT_H
