#include "clientworker.h"
#include <QTimer>
#include <QCoreApplication>
#include "proto.h"
#include "logger.h"

ClientWorker::ClientWorker() : m_socket(this)
{
    connect(&m_socket, &QTcpSocket::connected, this, &ClientWorker::connectToSrv);
    connect(&m_socket, &QTcpSocket::readyRead, this, &ClientWorker::dataArrived);
}

ClientWorker::~ClientWorker()
{

}

void ClientWorker::setParams(const QString &host, uint port, double value)
{
    m_hostAddr = host;
    m_hostPort = port;
    m_doubleValue = value;
}

// if we need to handle somehow parsing error. Nothing to do for now.
void ClientWorker::parsingError(const QByteArray &data)
{
    Q_UNUSED(data);
    return;
}

void ClientWorker::getData(quint32 packetNum, quint32 packetsTotal,
                           QByteArray payload)
{
    m_mutex.lock();
    m_receivedPackets[packetNum] = payload;
    m_mutex.unlock();
    emit onPacketReceived(packetNum, packetsTotal);
    m_socket.write(Protocol::requestPacketReceived(packetNum));
}

QByteArray ClientWorker::popPacket(uint num)
{
    m_mutex.lock();
    QByteArray result = m_receivedPackets.value(num);
    m_receivedPackets.remove(num);
    m_mutex.unlock();
    return result;
}

void ClientWorker::start()
{
    m_socket.connectToHost(m_hostAddr, m_hostPort);
}

void ClientWorker::connectToSrv()
{
    QTimer::singleShot(3000, this, &ClientWorker::timeoutAfterConnect);
}

void ClientWorker::timeoutAfterConnect()
{
    if(!m_socket.isOpen()){
        QString logMsg = QString("Error: Server closed connection unexpectedly."
                                 " Abort the program.");
        Logger::instance().log(logMsg);
        QCoreApplication::exit(); // or something else to do?
    }
    m_socket.write(Protocol::requestGetData(m_doubleValue));
    m_socket.flush();
}

void ClientWorker::dataArrived()
{
    QByteArray data = m_socket.readAll();
    Protocol::parseFromSrv(data, *this);
}
