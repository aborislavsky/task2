#ifndef CLIENTWORKER_H
#define CLIENTWORKER_H

#include <QObject>
#include <QTcpSocket>
#include <QMutex>
#include <QMap>
#include "proto.h"

class ClientWorker : public QObject, public IClientProtocol
{
    Q_OBJECT
public:
    ClientWorker();
    ~ClientWorker();

    void setParams(const QString &host, uint port, double value);
    void parsingError(const QByteArray &data);
    void getData(quint32 packetNum, quint32 packetsTotal, QByteArray payload);

    QByteArray popPacket(uint num);

public slots:
    void start();

private slots:
    void connectToSrv();
    void timeoutAfterConnect();
    void dataArrived();

signals:
    void onPacketReceived(int packetNum, int outOfPackets);

private:
    QString m_hostAddr; // server IP or hostname
    uint    m_hostPort; // server TCP port
    double  m_doubleValue; // value to be sent to server

    QTcpSocket m_socket;

    QMutex     m_mutex;
    QMap<uint, QByteArray> m_receivedPackets;
};

#endif // CLIENTWORKER_H
