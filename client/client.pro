TEMPLATE = app
CONFIG += console
QT += network
INCLUDEPATH += ../proto ../logger
SOURCES += main.cpp \
    clientworker.cpp \
    client.cpp
HEADERS += \
    clientworker.h \
    client.h

CONFIG(debug, debug|release){
    debug {win32-msvc* {LIBS += -L$${OUT_PWD}/../proto/debug -L$${OUT_PWD}/../logger/debug} }
} else {
    release {win32-msvc* {LIBS += -L$${OUT_PWD}/../proto/release -L$${OUT_PWD}/../logger/release} }
}

LIBS += -L$${OUT_PWD}/../proto -L$${OUT_PWD}/../logger -lproto -llogger
