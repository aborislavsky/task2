#include "client.h"
#include <QSettings>
#include <QFile>
#include <QCoreApplication>
#include <QStringList>
#include <QVariant>
#include <QTimer>
#include "logger.h"

static const QString sc_iniSection = "main"; // ini file section name
static const QString sc_paramHostAddr = "host"; // param name for host addr
static const QString sc_paramHostPort = "port"; // param name for TCP port num
static const QString sc_paramDouble = "doubleValue"; // param name for double

// default values for config file
static const QString sc_defaultHostAddr = "127.0.0.1";
static const uint    sc_defaultHostPort = 2727;
static const double  sc_defaultDouble = 27.27;

static const QString sc_cfgFname = "/client.ini"; // config filename in app dir
static const QString sc_outFname = "/client-out.bin"; // output filename

Client::Client(const QString &configFName) : m_confFile(configFName)
{
    m_worker.moveToThread(&m_workerThread);

    // run when main event loop is active
    QTimer::singleShot(0, &m_worker, &ClientWorker::start);

    connect(&m_worker, &ClientWorker::onPacketReceived,
            this, &Client::packetReceived);
    readCfgFile();
    m_worker.setParams(m_hostAddr, m_hostPort, m_doubleValue);

    m_outputFile.setFileName(QCoreApplication::applicationDirPath()
                             + sc_outFname);
    m_outputFile.remove();
    m_outputFile.open(QIODevice::WriteOnly);
    m_workerThread.start();
}

Client::~Client()
{
    m_workerThread.quit();
    m_outputFile.close();
    m_workerThread.wait();
}

void Client::readCfgFile()
{
    const QString conf = (!m_confFile.isEmpty() && QFile::exists(m_confFile))
            ? m_confFile
            : QCoreApplication::applicationDirPath() + sc_cfgFname;
    QSettings set(conf, QSettings::Format::IniFormat);
    set.beginGroup(sc_iniSection);
    if(QFile::exists(conf)){ // read config
        m_hostAddr = set.value(sc_paramHostAddr, sc_defaultHostAddr).toString();
        m_hostPort = set.value(sc_paramHostPort, sc_defaultHostPort).toUInt();
        m_doubleValue = set.value(sc_paramDouble, sc_defaultDouble).toDouble();
    } else { // create config file with default values
        set.setValue(sc_paramHostAddr, QString("%1").arg(sc_defaultHostAddr));
        set.setValue(sc_paramHostPort, QString("%1").arg(sc_defaultHostPort));
        set.setValue(sc_paramDouble, QString("%1").arg(sc_defaultDouble));
    }
    set.endGroup();
}

void Client::packetReceived(int packetNum, int outOfPackets)
{
    QByteArray packet = m_worker.popPacket(packetNum);

    if(!m_outputFile.isWritable()){
        QString logMsg = QString("Error: Could not write to file: %1! Abort the"
                                 " program.")
                .arg(m_outputFile.fileName());
        Logger::instance().log(logMsg);
        QCoreApplication::exit(); // or something else to do?
    }
    if(!m_outputFile.isOpen())
        // file may be closed by external reason (network FS?), let`s try to
        // reopen it in append mode
        if(!m_outputFile.open(QIODevice::Append)){
            QString logMsg = QString("Error: Could not open file: %1! Abort the"
                                     " program.")
                    .arg(m_outputFile.fileName());
            Logger::instance().log(logMsg);
            QCoreApplication::exit(); // or something else to do?
        }
    m_outputFile.write(packet);
    QString logMsg = QString("Status: Packet %1/%2 was written.")
            .arg(packetNum)
            .arg(outOfPackets);
    Logger::instance().log(logMsg);
    if(packetNum == outOfPackets){ // we have received all we need
        QCoreApplication::exit();
    }
}
