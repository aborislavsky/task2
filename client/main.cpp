#include <QCoreApplication>
#include <stdlib.h>
#include <QtGlobal>
#include "client.h"

int main(int argc, char *argv[])
{
    Q_UNUSED(argc); Q_UNUSED(argv);
    QCoreApplication a(argc, argv);
    Client client((argc > 1) ? argv[1] : QString());
    return a.exec();
}

