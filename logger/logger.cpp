#include "logger.h"
#include <QMutex>
#include <QMutex>
#include <QDebug>
#include <QDateTime>

class LoggerPrivate
{
public:
    LoggerPrivate() = default;

public:
    QMutex mutex;
};

Logger &Logger::instance()
{
    static Logger instance;
    return instance;
}

void Logger::log(const QString &msg)
{
    m_private->mutex.lock();
    qDebug() << QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss.zzz:")
            << msg;
    m_private->mutex.unlock();
}

Logger::Logger() : m_private(new LoggerPrivate())
{
}

Logger::~Logger()
{
    delete m_private;
    m_private = nullptr;
}
