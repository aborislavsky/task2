#ifndef LOGGER_H
#define LOGGER_H

class Logger
{
public:
    static Logger &instance();
    void log(const class QString &msg);

private:
    Logger();
    Logger(const Logger &logger) = delete;
    Logger & operator=(const Logger &ms) = delete;
    ~Logger();

private:
    class LoggerPrivate *m_private;
};

#endif // LOGGER_H
