QT       -= gui
TARGET = logger
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
        logger.cpp

HEADERS += \
        logger.h
