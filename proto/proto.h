#ifndef PROTO_H
#define PROTO_H

#include <QByteArray>
#include <QSysInfo>

class Protocol;
class IClientProtocol;
class IServerProtocol;

class Protocol
{
public:

    // encoders
    static QByteArray requestGetData(double requestNumeric);
    static QByteArray responseGetData(quint32 pcktNum, quint32 pcktTotal,
                                      const QByteArray &payload,
                                      bool toBigEndian);
    static QByteArray responseError(bool toBigEndian);
    static QByteArray requestPacketReceived(quint32 pcktNum);

    // parsers
    static void parseFromSrv(const QByteArray &data, IClientProtocol &client);
    static void parseFromClient(const QByteArray &data, IServerProtocol &srv,
                                QSysInfo::Endian clientByteOrder);

    static bool clientEndian(const QByteArray &data, QSysInfo::Endian &outCE);
};

// protocol parser would notify client about parsed responses from server
class IClientProtocol
{
public:
    virtual ~IClientProtocol() = default;
    virtual void parsingError(const QByteArray &data) = 0;
    virtual void getData(quint32 packetNum, quint32 packetsTotal,
                         QByteArray payload) = 0;
};

// protocol parser would notify server about parsed requests from client
class IServerProtocol
{
public:
    virtual ~IServerProtocol() = default;
    virtual void parsingError(const QByteArray &data) = 0;
    virtual void getData(quint64 doubleVal, QString &doubleValStr) = 0;
    virtual void packetReceived(quint32 packetNum) = 0;
};

#endif // PROTO_H
