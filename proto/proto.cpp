#include "proto.h"
#include <QString>
#include <QtEndian>
#include "logger.h"

enum PACKET_TYPE : quint32 {NONE, RESP_ERROR, REQ_GETDATA, RESP_GETDATA,
                            REQ_RECIEVED};

/*
    Server side is respecting client endianess, is able to receive packets both
    of them and responses in the same endianess as request was made with.
*/

#define encodeVar(p, var) { memcpy(p, &var, sizeof(var)); p += sizeof(var); }
#define decodeVar(p, var) { memcpy(&var, p, sizeof(var)); p += sizeof(var); }

/*      GetData request
 4 bytes (uint) packet type identifier
 8 bytes (double) double `as is` at client side
 ? bytes (zero ended string) string representation of double value client side
*/
QByteArray Protocol::requestGetData(double requestNumeric)
{
    const PACKET_TYPE packetType = PACKET_TYPE::REQ_GETDATA;
    const QString requestNumericStr = QString::number(requestNumeric, 'g', 16);
    QByteArray result(sizeof(packetType) + sizeof(requestNumeric)
                      + requestNumericStr.length() + 1, 0);
    char *p = result.data();;
    encodeVar(p, packetType);
    encodeVar(p, requestNumeric);
    QByteArray strBa = requestNumericStr.toLocal8Bit();
    memcpy(p, strBa.constData(), requestNumericStr.length());
    return result;
}


/*      GetData response
 4 bytes (uint) packet type identifier.
 4 bytes (uint) current packet number.
 4 bytes (uint) total packet count.
 4 bytes (uint) payload length in bytes.
 ? bytes (byte array) payload. Array of values as they would be seen on client
*/
QByteArray Protocol::responseGetData(quint32 pcktNum, quint32 pcktTotal,
                                     const QByteArray &payload, bool toBigEndian)
{
    const quint32 packetType = (toBigEndian)
            ? qToBigEndian<quint32>(PACKET_TYPE::RESP_GETDATA)
            : qToLittleEndian<quint32>(PACKET_TYPE::RESP_GETDATA);
    quint32 payloadSize = (toBigEndian)
            ? qToBigEndian<quint32>(payload.size())
            : qToLittleEndian<quint32>(payload.size());

    QByteArray result(sizeof(packetType) + sizeof(pcktNum)
                      + sizeof(pcktTotal) + sizeof(payloadSize)
                      + payloadSize, 0);
    char *p = result.data();
    encodeVar(p, packetType);
    encodeVar(p, pcktNum);
    encodeVar(p, pcktTotal);
    encodeVar(p, payloadSize);
    memcpy(p, payload.constData(), payload.size());
    return result;
}

/*      Error response
  4 bytes (uint) packet type identifier.
*/
QByteArray Protocol::responseError(bool toBigEndian)
{
    const quint32 packetType = (toBigEndian)
            ? qToBigEndian<quint32>(PACKET_TYPE::RESP_ERROR)
            : qToLittleEndian<quint32>(PACKET_TYPE::RESP_ERROR);
    QByteArray result(sizeof(packetType), 0);
    memcpy(result.data(), &packetType, sizeof(packetType));
    return result;
}

/*      PacketReceived request
 4 bytes (uint) packet type identifier.
 4 bytes (uint) received packet number.
*/
QByteArray Protocol::requestPacketReceived(quint32 pcktNum)
{
    const PACKET_TYPE packetType = PACKET_TYPE::REQ_RECIEVED;
    QByteArray result(sizeof(packetType) + sizeof(pcktNum), 0);
    char *p = result.data();
    encodeVar(p, packetType);
    encodeVar(p, pcktNum);
    return result;
}

void Protocol::parseFromSrv(const QByteArray &data, IClientProtocol &client)
{
    if(quint32(data.size()) < sizeof(PACKET_TYPE))
        return client.parsingError(data);
    const char *p = data.constData();
    PACKET_TYPE packetType;
    decodeVar(p, packetType);
    switch (packetType) {
    case PACKET_TYPE::RESP_GETDATA:{
        if(data.length() < 16)
            return client.parsingError(data);
        quint32 currPacketNum, packetTotalNum, packetLength;
        decodeVar(p, currPacketNum);
        decodeVar(p, packetTotalNum);
        decodeVar(p, packetLength);
        if(quint32(data.length()) < packetLength + 16) // not enough rcvd data
            return client.parsingError(data);
        QByteArray payload(p, packetLength);
        client.getData(currPacketNum, packetTotalNum, payload);
        break;
    }
    case PACKET_TYPE::RESP_ERROR:{
        QString logMsg = QString("Server could not handle badly formed request!");
        Logger::instance().log(logMsg);
        break;
    }
    default:
        client.parsingError(data);
        break;
    }
}

void Protocol::parseFromClient(const QByteArray &data, IServerProtocol &srv, QSysInfo::Endian clientByteOrder)
{
    if(quint32(data.size()) < sizeof(PACKET_TYPE))
        return srv.parsingError(data);
    const char *p = data.constData();
    quint32 packetType;
    decodeVar(p, packetType);
    packetType = (clientByteOrder == QSysInfo::Endian::LittleEndian)
            ? qToLittleEndian<quint32>(packetType)
            : qToBigEndian<quint32>(packetType);

    switch (packetType) {
    case PACKET_TYPE::REQ_GETDATA:{
        if(data.length() < 14)// pckt type + double + str(at least 1 symb + \0)
            return srv.parsingError(data);
        quint64 dbl;
        decodeVar(p, dbl);
        dbl = (clientByteOrder == QSysInfo::Endian::LittleEndian)
                ? qToLittleEndian<quint64>(dbl)
                : qToBigEndian<quint64>(dbl);
        QString str(p);
        srv.getData(dbl, str);
        break;
    }
    case PACKET_TYPE::REQ_RECIEVED:{
        if(data.length() < 8)// pckt type + pcktNum
            return srv.parsingError(data);
        quint32 packetNum;
        decodeVar(p, packetNum);
        packetNum = (clientByteOrder == QSysInfo::Endian::LittleEndian)
                ? qToLittleEndian<quint32>(packetNum)
                : qToBigEndian<quint32>(packetNum);
        srv.packetReceived(packetNum);
        break;
    }
    default:
        srv.parsingError(data);
        break;
    }
}

bool Protocol::clientEndian(const QByteArray &data, QSysInfo::Endian &outCE)
{
    QSysInfo::Endian srv = (QSysInfo::buildAbi().contains("little_endian"))
            ? QSysInfo::Endian::LittleEndian
            : QSysInfo::Endian::BigEndian;
    if(data.size() < int(sizeof(quint32)))
        return false;

    const char *p = data.constData();
    quint32 packetType;
    decodeVar(p, packetType);

    if(srv == QSysInfo::Endian::LittleEndian){
        if(packetType == PACKET_TYPE::REQ_GETDATA
                || packetType == PACKET_TYPE::REQ_RECIEVED){
            outCE = QSysInfo::Endian::LittleEndian;
            return true;
        } else if(packetType == qToBigEndian<quint32>(PACKET_TYPE::RESP_GETDATA)
                  || packetType == qToBigEndian<quint32>(PACKET_TYPE::RESP_ERROR)){
            outCE = QSysInfo::Endian::BigEndian;
            return true;
        }
        return false; // bad packet type
    } else {
        if(packetType == PACKET_TYPE::REQ_GETDATA
                || packetType == PACKET_TYPE::REQ_RECIEVED){
            outCE = QSysInfo::Endian::BigEndian;
            return true;
        } else if(packetType == qToLittleEndian<quint32>(PACKET_TYPE::RESP_GETDATA)
                  || packetType == qToLittleEndian<quint32>(PACKET_TYPE::RESP_ERROR)){
            outCE = QSysInfo::Endian::LittleEndian;
            return true;
        }
        return false; // bad packet type
    }

    return false; // should never be reached
}
