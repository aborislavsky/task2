#ifndef SESSION_H
#define SESSION_H

#include <QObject>
#include <QSysInfo>
#include <QThread>
#include <QTcpSocket>

#include "proto.h"
#include "responsecreator.h"

class Session : public QObject, public IServerProtocol
{
    Q_OBJECT
public:
    Session(qintptr socketDescriptor, ServerParams params);
    ~Session();
    void parsingError(const QByteArray &data);
    void getData(quint64 doubleVal, QString &doubleValStr);
    void packetReceived(quint32 packetNum);

private:
    void sendBufferedPacketsToClient();
    void deleteResponseCreator();

public slots:
    void initialise();

private slots:
    void dataArrived(); // on any data arrived on socket
    void responsePacketIsReady(uint packetNum); // we could add header and send it

    // all packets prepared we could add header and send all packets
    void responsePacketsAreReady(uint from, uint to);

private:
    QSysInfo::Endian m_srvByteOrder;    // byte order used by server

    // byte order used by client
    QSysInfo::Endian m_clientByteOrder;
    bool m_clientByteOrderIsDetermined;
    bool m_clientDoubleByteOrderSameAsOther;

    qintptr      m_socketDescriptor;
    QTcpSocket  *m_socket;

    ServerParams m_params;

    ResponseCreator *m_responseCreator;
    QThread         *m_responseCreatorThread;
    QMap<uint, QByteArray> m_bufferedPackets;

    uint m_lastSentPacketNum; // 0 means no packet have been sent yet
    uint m_lastDeliveredPacketNum; // 0 means no packet have been sent yet
    uint m_packetsTotal;

    QString m_clientName;
};

#endif // SESSION_H
