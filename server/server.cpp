#include "server.h"
#include <QSettings>
#include <QFile>
#include <QCoreApplication>
#include <QStringList>
#include <QVariant>
#include <QTimer>
#include <QThread>
#include "logger.h"

static const QString sc_iniSection = "main"; // ini file section name
static const QString sc_paramPort = "port"; // param name for TCP port
static const QString sc_paramValuesPerPckt = "valuesPerPacket";
static const QString sc_paramValuesTotal = "valuesTotal";
static const uint    sc_defaultPort = 2727;
static const uint    sc_defaultValuesPerPckt = 1000;
static const uint    sc_defaultValuesTotal = 1000000;
static const QString sc_cfgFname = "/server.ini"; // config filename in app dir

Server::Server(const QString &confFName, QObject *parent)
    : QTcpServer(parent)
{
    // read cfg
    const QString conf = (!confFName.isEmpty() && QFile::exists(confFName))
            ? confFName
            : QCoreApplication::applicationDirPath() + sc_cfgFname;
    QSettings set(conf, QSettings::Format::IniFormat);
    set.beginGroup(sc_iniSection);
    if(QFile::exists(conf)) { // read config
        m_port = set.value(sc_paramPort, sc_defaultPort).toUInt();
        m_params.valuesPerPacket = set.value(sc_paramValuesPerPckt,
                                           sc_defaultValuesPerPckt).toUInt();
        m_params.valuesTotal = set.value(sc_paramValuesTotal,
                                           sc_defaultValuesTotal).toUInt();
    } else { // create config file with default values
        set.setValue(sc_paramPort, QString("%1").arg(sc_defaultPort));
        set.setValue(sc_paramValuesPerPckt,
                     QString("%1").arg(sc_defaultValuesPerPckt));
        set.setValue(sc_paramValuesTotal,
                     QString("%1").arg(sc_defaultValuesTotal));
    }
    set.endGroup();

    listen(QHostAddress::Any, m_port);

    QString logMsg = QString("Server started and listen on %1 port. Server "
                             "thread %2.")
            .arg(m_port)
            .arg(QString().sprintf("%08p", thread()));
    Logger::instance().log(logMsg);
}

void Server::incomingConnection(qintptr socketDescriptor)
{
    Session *session = new Session(socketDescriptor, m_params);
    QThread *thread = new QThread();
    session->moveToThread(thread);
    thread->start();
    m_sessions[session] = thread;
    QTimer::singleShot(0, session, &Session::initialise);
    connect(session, &QObject::destroyed, this, &Server::removeSession);
}

// remove session from m_sessions when it is about to delete
void Server::removeSession(QObject *session)
{
    m_sessions.remove(static_cast<Session*>(session));
}


