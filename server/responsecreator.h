#ifndef RESPONSECREATOR_H
#define RESPONSECREATOR_H

#include <QObject>
#include <QMutex>
#include <QMap>
#include <QPair>
#include <QSysInfo>

typedef struct ServerParams
{
    uint valuesPerPacket;
    uint valuesTotal;
}ServerParams;

class ResponseCreator : public QObject
{
    Q_OBJECT
public:
    ResponseCreator(double startVal, ServerParams params,
                    QSysInfo::Endian endian, const QString &clientName);
    ~ResponseCreator();

    QByteArray packetPop(uint packetNum);
    uint packetsTotal();

private slots:
    void run();
    void packetPush(QByteArray &packet, uint packetNum);

signals:
    void packetReady(uint packetNum);
    void allPacketsAreReady(uint from, uint to);

private:
    ServerParams            m_params;   // some params from config file
    double                  m_startVal; // value client sends
    QSysInfo::Endian        m_endian;   // client endian
    uint                    m_packetsTotal;
    QMap<uint, QByteArray>  m_packets;
    QMutex                  m_packetsMutex;
    QString                 m_clientName;
};

#endif // RESPONSECREATOR_H
