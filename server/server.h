#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>
#include <QList>
#include <QThread>

#include "session.h"

class Server : public QTcpServer
{
    Q_OBJECT
public:
    explicit Server(const QString &confFName, QObject *parent = nullptr);

protected:
    void incomingConnection(qintptr socketDescriptor);

private slots:
    void removeSession(QObject *session);

private:
    uint m_port;
    ServerParams m_params;
    QMap<Session*, QThread*> m_sessions;
};

#endif // SERVER_H
