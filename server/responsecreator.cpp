#include "responsecreator.h"
#include <QTimer>
#include <QtEndian>
#include <QCoreApplication>
#include "logger.h"

ResponseCreator::ResponseCreator(double startVal, ServerParams params,
                                 QSysInfo::Endian endian,
                                 const QString &clientName)
    : QObject(),
      m_startVal(startVal), m_endian(endian), m_clientName(clientName)
{
    m_params = params;
    QTimer::singleShot(0, this, &ResponseCreator::run);
}

ResponseCreator::~ResponseCreator()
{
    QString logMsg = QString("Data has been prepared for %1. Response creator"
                             " thread %2.")
            .arg(m_clientName)
            .arg(QString().sprintf("%08p", thread()));
    Logger::instance().log(logMsg);
}

QByteArray ResponseCreator::packetPop(uint packetNum)
{
    m_packetsMutex.lock();
    QByteArray result(m_packets.value(packetNum));
    m_packetsMutex.unlock();
    return result;
}

uint ResponseCreator::packetsTotal()
{
    m_packetsMutex.lock();
    uint result = m_packetsTotal;
    m_packetsMutex.unlock();
    return result;
}

void ResponseCreator::run()
{
    QString logMsg = QString("Starting to prepare data for %1. Response creator"
                             " thread %2.")
            .arg(m_clientName)
            .arg(QString().sprintf("%08p", thread()));
    Logger::instance().log(logMsg);
    m_packetsTotal = m_params.valuesTotal / m_params.valuesPerPacket;
    if(m_params.valuesTotal % m_params.valuesPerPacket)
        ++m_packetsTotal;

    double currentDouble = m_startVal++;
    uint doubleSize = qMin(sizeof(double), sizeof(quint64));
    uint m_currentPacket = 1;
    while(m_currentPacket <= m_packetsTotal){
        uint valuesRemains = (m_currentPacket == m_packetsTotal)
                ? m_params.valuesTotal - m_params.valuesPerPacket
                  * (m_currentPacket - 1)
                : m_params.valuesPerPacket;
        QByteArray packet(sizeof(currentDouble) * valuesRemains, 0);
        char *p = packet.data();
        for(uint i = 0; i < valuesRemains; ++i){
            quint64 tmp = 0;
            memcpy(&tmp, &currentDouble, doubleSize);
            tmp = (m_endian == QSysInfo::Endian::LittleEndian)
                    ? qToLittleEndian<quint64>(tmp)
                    : qToBigEndian<quint64>(tmp);
            memcpy(p, &tmp, doubleSize);
            p += doubleSize;
            ++currentDouble;
        }
        packetPush(packet, m_currentPacket);
        ++m_currentPacket;
    }

    // session could take all prepared packets
    m_packetsMutex.lock();
    uint from = m_packets.firstKey();
    uint to = m_packets.lastKey();
    m_packetsMutex.unlock();
    emit allPacketsAreReady(from, to);
}

// packet ready - push it to container and notify session
void ResponseCreator::packetPush(QByteArray &packet, uint packetNum)
{
    QByteArray pckt(packet);
    m_packetsMutex.lock();
    m_packets[packetNum] = pckt;
    m_packetsMutex.unlock();
    emit packetReady(packetNum);
}
