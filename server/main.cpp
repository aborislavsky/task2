#include <stdlib.h>
#include <QtGlobal>
#include <QCoreApplication>

#include "server.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Server server((argc > 1) ? argv[1] : QString());
    return a.exec();
}

