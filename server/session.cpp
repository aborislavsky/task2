#include "session.h"
#include <QCoreApplication>
#include <cstdlib>
#include <QtEndian>
#include <QHostAddress>
#include "logger.h"

Session::Session(qintptr socketDescriptor, ServerParams params)
    : QObject(),
      m_srvByteOrder((QSysInfo::buildAbi().contains("little_endian"))
                     ? QSysInfo::Endian::LittleEndian
                     : QSysInfo::Endian::BigEndian),
      m_clientByteOrderIsDetermined(false),
      m_socketDescriptor(socketDescriptor), m_socket(nullptr),
      m_responseCreator(nullptr),
      m_lastSentPacketNum(0), m_lastDeliveredPacketNum(0), m_packetsTotal(0)
{
    m_params = params;
}

Session::~Session()
{
    QString logMsg = QString("Job done for %1. Session thread %2.")
            .arg(m_clientName)
            .arg(QString().sprintf("%08p", thread()));
    Logger::instance().log(logMsg);

    deleteResponseCreator();
    m_socket->close();
    delete m_socket;
}

// if we need to handle somehow parsing error. Nothing to do for now.
void Session::parsingError(const QByteArray &data)
{
    Q_UNUSED(data);
    return;
}

void Session::getData(quint64 doubleVal, QString &doubleValStr)
{
    if(m_responseCreator){
        QString logMsg = QString("Server already working on response, while got"
                                 " a new request.");
        Logger::instance().log(logMsg);
        return;
    }

    // determine double encoding on client side
    quint64 revertedDoubleVal =
            (m_clientByteOrder == QSysInfo::Endian::LittleEndian)
            ? qToBigEndian<quint64>(doubleVal)
            : qToLittleEndian<quint64>(doubleVal);
    double doubleFromStr = doubleValStr.toDouble();
    double straigthDouble, revertedDouble;
    memcpy(&straigthDouble, &doubleVal, sizeof(double));
    memcpy(&revertedDouble, &revertedDoubleVal, sizeof(double));
    if(std::abs(straigthDouble - doubleFromStr) < 0.001)
        m_clientDoubleByteOrderSameAsOther = true;
    else if(std::abs(revertedDouble - doubleFromStr) < 0.001)
        m_clientDoubleByteOrderSameAsOther = false;
    else {
        QString logMsg = QString("Received request from client with unexpected"
                                 " architecture. That client would be ignored.");
        Logger::instance().log(logMsg);
        return;
    }

    double startDouble = (m_clientDoubleByteOrderSameAsOther)
            ? straigthDouble : revertedDouble;

    // should we make an extra convertion for double value for this client
    QSysInfo::Endian clientDoubleEndian = (m_clientDoubleByteOrderSameAsOther)
            ? m_clientByteOrder
            : ((m_clientByteOrder == QSysInfo::Endian::LittleEndian)
               ? QSysInfo::Endian::BigEndian : QSysInfo::Endian::LittleEndian);

    m_responseCreator = new ResponseCreator(startDouble, m_params,
                                            clientDoubleEndian, m_clientName);
    m_responseCreatorThread = new QThread();
    m_responseCreator->moveToThread(m_responseCreatorThread);
    m_responseCreatorThread->start();

    connect(m_responseCreator, &ResponseCreator::packetReady,
            this, &Session::responsePacketIsReady);
    connect(m_responseCreator, &ResponseCreator::allPacketsAreReady,
            this, &Session::responsePacketsAreReady);
}

void Session::packetReceived(quint32 packetNum)
{
    m_bufferedPackets.remove(packetNum);
    m_lastDeliveredPacketNum = packetNum;
    if(m_lastDeliveredPacketNum <= m_packetsTotal)
        sendBufferedPacketsToClient();
    else deleteLater();
}

void Session::sendBufferedPacketsToClient()
{
    if(!m_packetsTotal)
        if(m_responseCreator)
            m_packetsTotal = m_responseCreator->packetsTotal();

    QByteArray packet = m_bufferedPackets.value(m_lastSentPacketNum + 1);
    if(!packet.isEmpty()){
        QByteArray fullPacket = Protocol::responseGetData(
                    ++m_lastSentPacketNum,
                    m_packetsTotal,
                    packet,
                    m_clientByteOrder == QSysInfo::Endian::BigEndian);
        m_socket->write(fullPacket);
        m_socket->flush();
    }
}

void Session::deleteResponseCreator()
{
    if(m_responseCreator && m_responseCreatorThread){
        delete m_responseCreator;
        m_responseCreator = nullptr;
        m_responseCreatorThread->quit();
        m_responseCreatorThread->wait();
        delete m_responseCreatorThread;
        m_responseCreatorThread = nullptr;
    }
}

void Session::initialise()
{
    if(m_socket)
        delete m_socket;
    m_socket = new QTcpSocket(this);
    m_socket->setSocketDescriptor(m_socketDescriptor);
    connect(m_socket, &QTcpSocket::readyRead, this, &Session::dataArrived);
    connect(m_socket, &QTcpSocket::disconnected, this, &Session::deleteLater);

    m_clientName = QString("%1:%2").arg(m_socket->peerName())
            .arg(m_socket->peerAddress().toString());

    QString logMsg = QString("New connection from %1. Session thread %2.")
            .arg(m_clientName)
            .arg(QString().sprintf("%08p", thread()));
    Logger::instance().log(logMsg);
}

void Session::dataArrived()
{
    QByteArray data = m_socket->readAll();
    if(!m_clientByteOrderIsDetermined){
        if(!Protocol::clientEndian(data, m_clientByteOrder))
            return; // bad packet
        m_clientByteOrderIsDetermined = true;
    }
    Protocol::parseFromClient(data, *this, m_clientByteOrder);
}

void Session::responsePacketIsReady(uint packetNum)
{
    if(m_responseCreator){
        QByteArray packet = m_responseCreator->packetPop(packetNum);
        if(!packet.isEmpty())
            m_bufferedPackets[packetNum] = packet;
    }
    if(m_lastDeliveredPacketNum == m_lastSentPacketNum)
        sendBufferedPacketsToClient();
}

void Session::responsePacketsAreReady(uint from, uint to)
{
    if(!m_responseCreator)
        return;
    for(uint i = from; i <= to; ++i){
        QByteArray packet = m_responseCreator->packetPop(i);
        if(!packet.isEmpty())
            m_bufferedPackets[i] = packet;
    }
    deleteResponseCreator();
    sendBufferedPacketsToClient();
}
